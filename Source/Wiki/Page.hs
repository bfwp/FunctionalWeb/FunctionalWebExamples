{-# Language OverloadedStrings #-}

module Wiki.Page where

import qualified Data.Text as T

-- | A single Wiki page. We use "Data.Text" because that will support
-- every language that <http://www.unicode.org Unicode> supports.
data Page = Page {
  -- | The page title, also used to identify a page when linking from others.
  title :: T.Text,

  -- | The contents of the page. This
  content :: T.Text
  }

instance Show Page where
  show p = T.unpack $ T.intercalate "\n\n" [(title p), (content p)]
