{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Main where

import           Control.Applicative
import           Control.Exception (SomeException, try)
import           Control.Lens.TH
import           Snap
import           Snap.Snaplet.Config
import           System.IO
import qualified Data.Text as T

data Hello = Hello
data Goodbye = Goodbye

data Site = Site {
    _hello :: Snaplet Hello,
    _goodbye :: Snaplet Goodbye
    }

makeLenses ''Site

helloInit :: SnapletInit b Hello
helloInit = makeSnaplet "hello" "Hello" Nothing $ do
    addRoutes [("", h)]
    return Hello
        where
            h = writeBS "Hello World!"

goodbyeInit :: SnapletInit b Goodbye
goodbyeInit = makeSnaplet "goodbye" "Goodbye" Nothing $ do
    addRoutes [("", g)]
    return Goodbye
        where
            g = writeBS "Goodbye!"

site :: SnapletInit b Site
site = makeSnaplet "App" "Application" Nothing $ 
       Site <$> nestSnaplet "hello" hello helloInit 
              <*> nestSnaplet "goodbye" goodbye goodbyeInit

-- Main: snaplet runner
main :: IO ()
main =
    commandLineAppConfig defaultConfig
    >>= runSite

runSite :: Config Snap AppConfig -> IO ()
runSite conf =
    runSnaplet (appEnvironment =<< getOther conf) site
    >>= \(mm, si, cu) ->
            do hPutStrLn stderr $ T.unpack mm
    >> serve conf si >> cu

serve ::
    Config Snap a ->
    Snap () ->
    IO (Either SomeException ())
serve c s =
    try $ httpServe c s 
