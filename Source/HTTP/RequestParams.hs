{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Snap.Core
import           Snap.Http.Server
import           Data.ByteString (intercalate)

main :: IO ()
main = quickHttpServe . withRequest $
       maybe noHelloParam writeHellos . getHelloParam
    where
        noHelloParam =
          writeBS "No `hello' parameter."
        writeHellos =
          writeBS . intercalate "|"
        getHelloParam =
          rqParam "hello"
