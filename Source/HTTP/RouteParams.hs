{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative
import           Snap.Core
import           Snap.Http.Server
import           Data.ByteString (intercalate)

main :: IO ()
main = quickHttpServe $ route [
    ("hello/", hello),
    ("hello/:world", hello),
    ("goodbye", writeBS "Goodbye!")] 
       <|> writeBS "Sorry!"
        where
            hello = withRequest $
                    maybe noWorld world . getWorld
                where
                    getWorld = rqParam "world"
                    world = writeBS . intercalate "|"
                    noWorld = writeBS "No world!"
