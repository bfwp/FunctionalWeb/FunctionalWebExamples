{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Snap.Core
import           Snap.Http.Server

main :: IO ()
main = quickHttpServe showRequestURIPointful
    where
           showRequestURIPointful = do
             r <- getRequest
             let uri = rqURI r
             writeBS uri
             
